# Barnraiser-Dutch

## About Barnraiser

You can read more about the background of this repository here: [About_Barnraiser](About_Barnraiser.md)

The following is the original description of *Dutch*, taken from its homepage http://barnraiser.org/dutch

---

## Introducing Dutch
Share information quickly and easily with Dutch; our knowledge sharing network tool.

Dutch is a part of our research to re-think the way we share knowledge on the web. Our goal is to create a fluid pool of knowledge shared amoungst
interested people based upon them working together in gathering information from the web.

## Features
* Install as a single blog or a service to host many separate blogs.
* Lightweight easy to use interface.
* Simple creation of tag based networks.
* Networks favourites listing.
* Notification from your favourite networks.
* Email digest of favourite networks.
* OpenID support.
* Parse Digg items directly into the network.
* Parse Youtube movies into the network.
* Themed "skins" which can be easily downloaded and added.
* Multi-lingual.
* Free (GPL) software license

## Technical considerations
Dutch requires a web server running either Apache 1.3/2.x or IIS5/IIS6 with PHP5.x installed including GD library and Gettext (Curl and BCMath if you
want OpenID support).

For multiple instances you will require access to sub-domains.

---

There's also a very comprehensive user's manual available at http://barnraiser.org/dutch_guide which I copied
[here](documents/dutch_guide.html) for backup purposes.